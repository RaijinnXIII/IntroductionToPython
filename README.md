# Introduction to Python

Welcome to *Introduction to Python*, a beginner's course in Python 3 programming, written by Ryan Ferguson of **Raijinn Technologies, LLC**, an organization changing technical education with empathy & practical application.

Thank you for choosing my content to learn Python. Comments, critique, and other feedback are always appreciated.

Please direct comments to **ryan@raijinn.xyz**

---
---

## Table of Contents

#### General
- **Setup.md** -- Setting up the Python development environment
- **HowTo.md** -- How to accomplish certain tasks or find out certain information

#### Data Model
- **Strings.md** -- Using, manipulating, and digging into Strings
- **Lists.md** -- Using, manipulating, and accessing values in Lists.
