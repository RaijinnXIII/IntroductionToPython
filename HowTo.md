### *How to display information visually and with evaluated formatting?*

Use the global `print()` function to display information messages from a program.

```python
print("Displayed string data!")

# // Displayed string data!

formatted_string = "String with\n newline sequences\n included"
print(formatted_string)

# // String with
# //  newline sequences
# //  included
```

### *How do I know what each function does?*

Use the `print()` function on a given function's `__doc__` string, which every built-in function of Python has included, even the `print()` function itself.

```python
print(print.__doc__)

# print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)
#
# Prints the values to a stream, or to sys.stdout by default.
# Optional keyword arguments:
# file:  a file-like object (stream); defaults to the current sys.stdout.
# sep:   string inserted between values, default a space.
# end:   string appended after the last value, default a newline.
# flush: whether to forcibly flush the stream.
```

### *How do I know the data type of a value?*

**Syntax** -- aka, how something is written -- is how you can know on-sight whether something is an integer, string, or something else. If it's surrounded by quotations (single or double), it's a string. If it's a number with a decimal, it's a float. If it's got square brackets, a list. Curly brackets, then a dictionary, and so on.

If some value's syntax is hidden, probably because it's assigned to a variable elsewhere in a program, you can locate the variables type by using the global `type()` function.

```python

referenced_value = "My string value in another file"
type(referenced_value)

# <class 'str'>
```

This is helpful when you want to return the `class` object itself to continue using it, but if you just want to verify if some value is of a specific type, you can use the `isinstance()` function to return a `True/False` boolean value for use in a conditional statement.

```python

possible_string_value = "Another string that we aren't sure is a string yet"

isinstance(possible_string_value, str)

# True
```
