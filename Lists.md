# Lists

A *list* or *array* is an ordered, sequence of values -- and this order is called the "indexing" of an array. Each item in a list or array, by definition of being within an array, is given an index value dependent on its position within the list.

If a list of pet names existed as `pets = ["Sophie", "Moonbeam", "Rikku"]` and I wanted to know the index of the `"Sophie"` element, I would use: `pets.index("Sophie")` and the value returned would be `0` since "Sophie" is the first item in the list.

This is a helpful way to access elements when you know the value exists in the list, but do not know its location in the list.

Locating an item in an array can be done logically with a conditional, like so:

```python
pets = ["Sophie", "Moonbeam", "Rikku"]

if "Sophie" in pets:
  pet_index = pets.index("Sophie")
  print("Found Sophie in index: {i}".format(i=pet_index))

# // 'Found Sophie in index: 0'
```

**Pro Tip:** Lists always start with the index of zero, not one.



# Searching for Values

Lists can get long, and even if they're not, you don't want to try and memorize what's contained in a given array of data. In order to 'find out' if a given value exists in a list, there are logical operators to use for this.

If you know the value you want to look for and want to know if a given value is present in a list...

```python

search_term = "Billy Joe"
existing_data_set = ["Ryan", "Theresa", "Jane", "Thom", "George", "Anna", "Billy Joe"]

search_term in existing_data_set

# // True
```

If you know the value is present, but now want to find out what index it's stored at...

```python
search_term = "Anna"
existing_data_set = ["Ryan", "Theresa", "Jane", "Thom", "George", "Anna", "Billy Joe"]

existing_data_set.index(search_term)

# // 5
```

These two processes can be combined into a single logical block of code, like so:

```python
search_term = "Anna"
existing_data_set = ["Ryan", "Theresa", "Jane", "Thom", "George", "Anna", "Billy Joe"]

if search_term in existing_data_set:
  index_located = existing_data_set.index(search_term) # // 5
  print("Found term '{term}' at index: {index}".format(term=search_term, index=index_located))
else:
  print("Could not locate search term in data set")

# // Found term 'Anna' at index: 5
```

# Manipulating Lists

List manipulation is preferred when operating in a uniform way across a many values, for instance, lowercasing each instance of a string within a data set to normalize the format. If you've ever let 100 different people fill out a form or spreadsheet, you'll notice that everyone has their intepretation of how the data should look.

```python



```
