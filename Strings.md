# Strings

Strings are a data type intended to represent human-readable information, like an English word, name, or message to be displayed at some point. Strings are considered *sequences*, like lists, meaning each element in a string -- each character -- can be accessed by the index of the item, as if it were in a list.

For instance: `"Ryan"[0]` would output `'R'` because it's the first item in the array, which is the zero index.

Read more about strings here: Official Python Docs [Link](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str)

## Evaluating Strings

*Evaluating* occurs when a string is "returned" or *executed*, rather than being assigned to a variable or used within the limited scope of a function. Once a value is "evaluated" to its containing scope, it is destroyed unless stored somewhere, like a variable or a file.

```python
# Evaluate a string value
"This is a string value. Human-readable and all that."

# // 'This is a string value. Human-readable and all that.'

# <-----  ----->
# Evaluate an empty string value
""

# // ''

# <-----  ----->
# Evaluate an empty string with global function
str()

# // ''
```

## Defining Strings

Strings can be *defined* or *assigned* by providing a value enclosed in single quotations or double quotations, as long as they are the same when opening & closing for a string value, a. String values can also be defined by calling the global `str()` initialization function, which can be passed a value to initialize with.

```python
# <-----  ----->
# Define a string value
my_string_variable = "Hello, learner of Python."

# <-----  ----->
# Define empty string value (#1)
an_empty_string = ''

# // ''

# <-----  ----->
# Define empty string value (#2)
another_empty_string = str()

# // ''

# <-----  ----->
# Initialize string with value
my_string_with_default_value = str("Ryan")

```

## Combining Strings

The most common methods of combining multiple strings together are *concatenation* and *interpolation*.



### Concatenation
```python

# <----- ----->
# Evaluate a concatenated string
"This " + "is string concatenation."

# <----- ----->
# Assign a concatenated string
first_name = "Ryan"
last_name = "Ferguson"
full_name = first_name + " " + last_name

print(full_name)

// 'Ryan Ferguson'
```

### Interpolation

```python
# <----- ----->
# Evaluate an interpolated string
"This is a string with data in it: {data}".format(data=1)

# <---- ----->
# Assign an interpolated string
template = "First Name: {fname}".format(fname=first_name)

print(template)

# // 'First Name: Ryan'
```
