## Project Setup -- *"Where our Python files are located..."*

1. Create a new folder in your Desktop called `IntroPython.py`
2. To check if Python is currently installed by attempting to run this file:
    * **MacOS & Linux** `python $HOME/Desktop/IntroPython.py`
    * **Windows:** `python C:\Users\$(env:UserName)\Desktop/IntroPython.py`
3. If it does nothing, it worked!
    * If an error message happens, let's talk about it!


## IDE Setup -- *"How we'll edit Python files..."*

  `IDE: Interactive Development Environment`

**Note:** Any IDE will do, like Atom, Sublime, but we'll be using **Visual Studio Code** in the setup if an alternative is not preferred and already installed.

1. Install Visual Code Studio - [Download](https://code.visualstudio.com/)
2. Click, "yes" through the installation prompts and accept default settings.
3. Click *File* from the top menu with Visual Studio Code open
    * 3a. Choose *Open...* from the *File* menu dropdown
    * 3b. Locate the folder we just created in the file search menu.
4. You should see the folder and contained files in the *File Explorer* in Visual Studio Code
5. If you can, click the `IntroPython.py` file to open it for editing.
6. Upon opening the file, VSC should throw a pop-up or two, one of which asks to install a Python stynax extension.
    - Go ahead and install that extension.
7. Once it's installed, let's continue.


## Checking if Python is installed...

Before we install anything, let's find out if Python is already installed on your machines. If the following steps conclude you do not have Python installed, continue to "Installing Python," otherwise, skip ahead to "Upgrading Python."

**Windows 8/10**

1. Search for the `Windows Powershell` application & open it.
2. Type `python` and press Enter.
3. If you get a long message in red text, you don't have it installed.
4. If you see many long messages in white text, it's installed and you've just opened it.
    - Enter `exit()` to exit the Python interpreter.

----

**MacOS & Linux**

1. Search for the `Terminal` application & open it.
2. Type `python` and press Enter.
3. If you get a message describing a 'command not found', you don't have it installed.
4. If you see long messages in white text, it's installed and you've just opened it.
    - Enter `exit()` to exit the Python interpreter.
    
## Installing Python

Official Python Downloads - [Link](https://www.python.org/downloads/)
