# Built-in Functions

*Functions* are operations which can be completed programmatically, such as adding two numbers together with the `1 + 1` syntax, known as a *token operator* since the functionality exists within the `+` token, rather than a typical function call.

Some of the most commonly seen functions are `print()`, which is used to display string values that may or may not include runtime formatting evaluation, like when interpolating string values. In fact, let's look at the format function, which is a `method` of the `str` data class.

## Initialization Functions

A method is a function that is bound to a particular `class`; a built-in class could be any standard data type in Python. For instance, `str`, `int`, `float`, `list`, `dict`, etc. are all standard built-in classes, and can be *called* to "initialize" an instance of that class.

```python
# <-----  ----->
# Evaluate the "str" class, but do not create one
str

# // <class 'str'>

# <-----  ----->
# Initialize the "str" class to create a string object
str()

# // ''
```

The first example simply affirms that a class called "str" does in fact exist, nothing else. The second example actually creates a string object by calling the `str()` function, which does just that. Since we did not pass any arguments to the `str()` function, an empty string was created. Let's create one the same way, but with an initial value.

```python
# <-----  ----->
# Initialize the "str" class with initial value
str("Hello")

# // 'Hello'
```

This is also a good way to force a piece of data, which may not be a string, to *become* one. This is called "data type coercion," and it's something you may employ when receiving undesired data types from customers, the web, a data set, or your own code.

```python

not_a_string_variable = 123.15
now_is_a_string = str(not_a_string_variable)

not_a_string_variable
# // 123.15

now_is_a_string
# // '123.15'
```
## Informational Functions

Some functions are meant to create new data, mutate existing data, and some are meant to inform the Developer -- you -- of certain information and give you access to that information in your programs. For instance, operating improperly on data types will often cause a `TypeError`, but *knowing* things about your data first and making needed changes keeps from having broken code. Let's look at some helpful, informational functions.


#### dir()

dir() is a function which displays the contents of a given module, built-in or user-defined, it doesn't matter.

If we pass in a string object, dir() will tell us everything *inside* this particular string, and all the contents *inherited* by virtue of being a string to start with.

```python
dir("hello")

# /// ['__add__', '__class__', '__contains__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mod__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmod__', '__rmul__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'capitalize', 'casefold', 'center', 'count', 'encode', 'endswith', 'expandtabs', 'find', 'format', 'format_map', 'index', 'isalnum', 'isalpha', 'isascii', 'isdecimal', 'isdigit', 'isidentifier', 'islower', 'isnumeric', 'isprintable', 'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'maketrans', 'partition', 'replace', 'rfind', 'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split', 'splitlines', 'startswith', 'strip', 'swapcase', 'title', 'translate', 'upper', 'zfill']

```

#### isinstance() & type()

When you want to make sure a piece of data is of a certain type, there are some built-in functions which can be used to accomplish this. Most answers to "which one should I use?" are dependent on what type of value are you wanting back?

To illustrate the differences:

```python
value_in_question = "A string value!"

isinstance(value_in_question, int)
# // False

isinstance(value_in_question, str)
# // True
```

```python
value_in_question = ["BMW", "Mercedes", "Porsche", "Lambo"]
type(value_in_question)

# // <class 'list'>
```

The difference here, is that `isinstance()` helps you determine a logical truth or falsehood and `type()` simply returns the class of the argument passed.
